package by.gsu.igi.students.GolovinaAnastasiya.lab5;

/**
 * Created by agolovina on 22.12.16.
 */
public class Cat {
    public class Cat implements Speakable {
        @Override
        public void speak() {
            System.out.println("meow");
        }
    }
}