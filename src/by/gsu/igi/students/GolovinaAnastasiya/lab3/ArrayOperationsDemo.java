package by.gsu.igi.students.GolovinaAnastasiya.lab3;

import java.util.Scanner;

/**

  * Created by Golovina Anastasiya

 */



public class ArrayOperationsDemo {

    public static int findMax(int[] numbers) {
        int max = numbers[0];

        for (int i = 1; i < numbers.length; i++) {
            if (numbers[i] > max) {
                max = numbers[i];
            }
        }

        return max;
    }

    public static int findMin(int[] numbers) {
        int min = numbers[0];

        for (int i = 1; i < numbers.length; i++) {
            if (numbers[i] < min) {
                min = numbers[i];
            }
        }

        return min;
    }

    public static int calculateAverage(int[] numbers) {
        int avg = 0;

        for (int number : numbers) {
            avg = avg + number;
        }

        return (avg / numbers.length);
    }

    public static int product(int[] numbers) {

        int product = 1;
        for (int number : numbers) {
            product = product * number;
        }
        return product;
    }

    public static int sum(int[] numbers) {

        int sum = 0;
        for (int number : numbers) {
            sum = sum + number;
        }

        return sum;
    }

    public static int difference(int[] numbers) {

        int dif = 0;
        for (int number : numbers) {
            dif = dif - number;
        }

        return dif;
    }

    public static void main(String[] args) {
        // читаем из консоли последовательность целых чисел,
        int[] numbers = readArray();

        // вызываем вычислительные методы для полученного массива
        processArray(numbers);
    }

    private static int[] readArray() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите количество элементов:");
        int size = scanner.nextInt();

        int[] numbers = new int[size];
        for (int i = 0; i < size; i++) {
            System.out.println("Введите " + (i + 1) + "-й элемент:");
            numbers[i] = scanner.nextInt();
        }

        return numbers;
    }

    private static void processArray(int[] numbers) {
        int max = findMax(numbers);
        System.out.println("Максимальный элемент: " + max);

        int min = findMin(numbers);
        System.out.println("Минимальный элемент: " + min);

        int average = calculateAverage(numbers);
        System.out.println("Среднее значение: " + average);

        int product = product(numbers);
        System.out.println("Произведение элементов: " + product);

        int sum = sum(numbers);
        System.out.println("Сумма элементов: " + sum);

        int difference = difference(numbers);
        System.out.println("Разность элементов: " + difference);
    }
}
