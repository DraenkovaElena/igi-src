package by.gsu.igi.students.EkaterinaPopovich.lab5;

/**
 * Created by epopovich on 15.12.16.
 */
public interface Speakable {
    void speak();
}

