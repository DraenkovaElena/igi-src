package by.gsu.igi.students.AleksandraSemenova.lab5;

/**
 * Created by Aleksandra Semenova.
 */
public class Horse implements Speakable {
    @Override
    public void speak() {
        System.out.println("I-go-go");
    }
}
