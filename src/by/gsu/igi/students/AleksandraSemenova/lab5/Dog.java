package by.gsu.igi.students.AleksandraSemenova.lab5;

/**
 * Created by Aleksandra Semenova.
 */
public class Dog implements Speakable {
    @Override
    public void speak() {
        System.out.println("Vuf-vuf");
    }
}
