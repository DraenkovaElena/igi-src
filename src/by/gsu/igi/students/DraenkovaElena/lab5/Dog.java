package by.gsu.igi.students.DraenkovaElena.lab5;

/**
 * Created by DraenkovaElena.
 */
public class Dog implements Speakable {
    @Override
    public void speak() {
        System.out.println("Vuf-vuf");
    }
}
