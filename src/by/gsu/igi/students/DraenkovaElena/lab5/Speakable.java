package by.gsu.igi.students.DraenkovaElena.lab5;

/**
 * Created by DraenkovaElena.
 */
public interface Speakable {
    void speak();
}
