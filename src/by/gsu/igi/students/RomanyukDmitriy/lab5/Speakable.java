package by.gsu.igi.students.RomanyukDmitriy.lab5;

/**
 * Created by Romanyuk Dmitriy.
 */
public interface Speakable {
    void speak();
}
