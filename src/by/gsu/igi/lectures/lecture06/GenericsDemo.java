package by.gsu.igi.lectures.lecture06;

import java.util.ArrayList;
import java.util.List;

/**
 * Author: Evgeniy Myslovets
 */
public class GenericsDemo {

    public static void main(String[] args) {
        List<String> list = createList();

        for (String str : list) {
            System.out.println("Is string " + str + " empty? - " + isEmpty(str));
        }

        System.out.println(list);
    }

    private static List<String> createList() {
        List<String> list = new ArrayList<>();
        list.add("One");
        list.add("2");
        return list;
    }

    public static boolean isEmpty(String str) {
        return str.length() == 0;
    }
}
