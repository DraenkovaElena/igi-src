package by.gsu.igi.lectures.lecture10;

import javax.swing.*;
import java.awt.*;

/**
 * Created: 21.11.2016
 *
 * @author Evgeniy Myslovets
 */
public class MySimpleDrawPanel extends JPanel {

    @Override
    protected void paintComponent(Graphics g) {
        g.setColor(Color.blue);
        g.fillRect(20, 50, 100, 100);
    }
}
